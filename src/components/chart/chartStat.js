import React, { useEffect, useRef } from 'react'
import PropTypes from 'prop-types'
import Chart from 'chart.js'
import { config } from './index';

let chart

const ChartStat = ({ labels, data }) => {

  const canvas = useRef()
  config.data.labels = labels
  config.data.datasets[0].data = data

  useEffect(() => {
    let ctx = canvas.current.getContext('2d')
    chart = new Chart(ctx, config)
  }, [labels, data])

  useEffect(() => {
    chart.update()
  })

  return (
    <div>
      <canvas
        ref={canvas}
        width="400"
        height="400"
      />
    </div>
  )
}

ChartStat.propTypes = {
  labels: PropTypes.array,
  data: PropTypes.array
}

export default ChartStat