const config = {
  type: 'line',
  data: {
    labels: [],
    datasets: [
      {
        label: 'Custom dataset',
        backgroundColor: 'rgb(255, 99, 132, 0.2)',
        borderColor: 'rgb(255, 99, 132, 1)',
        data: [],
        pointRadius: 5,
        pointHoverRadius: 7,
        pointHitRadius: 5,
        pointBorderWidth: 2,
        pointStyle: 'rectRounded',
        borderWidth: 3
      }
    ]
  },
  options: {
    title: {
      display: true,
      text: 'Custom Chart Title'
    },
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true,
          // stepSize: 2,
          // suggestedMin: 70000,
          suggestedMax: 500
        }
      }]
    }
  }
}

export default config