export const body = {
  "query": {
    "range": {
      "pubDate": {
        "gte": "23/05/2020",
        "lte": "29/11/2020",
        "format": "dd/MM/yyyy"
      }
    }
  },
  "aggs": {
    "docs_count_aggregate": {
      "date_histogram": {
        "field": "pubDate",
        "calendar_interval": "week"
      }
    }
  }
}

export const body1 = {
  "query": {
    "range": {
      "pubDate": {
        "gte": "01/04/2020",
        "lte": "29/11/2020",
        "format": "dd/MM/yyyy"
      }
    }
  },
  "aggs": {
    "docs_count_aggregate": {
      "date_histogram": {
        "field": "pubDate",
        "calendar_interval": "day"
      }
    }
  }
}