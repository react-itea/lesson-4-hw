export const useFetch = () => {

  const username = 'elastic';
  const password = '123456';
  // let url = `http://188.214.30.108:9200/rbst-retro/_count`
  const authString = `${username}:${password}`
  let headers = new Headers();
  headers.set('Authorization', 'Basic ' + btoa(authString))

  const request = async (url, method = 'GET', body) => {

    try {
      if (body) {
        body = JSON.stringify(body)
        // headers['Content-Type'] = 'application/json'
        headers.set('Content-Type', 'application/json')
      }
    } catch (error) {
      throw error
    }

    const response = await fetch(url, { method: method, body: body, headers: headers })
    const data = await response.json()
    return data
  }
  return { request }
}