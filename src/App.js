import { useCallback, useEffect, useState } from 'react';
import { ChartStat } from './components/chart';
import { useFetch } from './hooks/useFetch.hook'
import { body, body1 } from './query';

function App() {

  const [chartData, setChartData] = useState([])
  const [labels, setLabels] = useState([])

  const { request } = useFetch()

  const reducer = (accum, current) => {
    accum.labels.push(current.key_as_string)
    accum.dataSet.push(current.doc_count)
    return accum
  }
  const fetchData = useCallback(async (body) => {
    const data = await request('http://188.214.30.108:9200/_search?size=0', 'POST', body)
    const { buckets } = data.aggregations.docs_count_aggregate
    const newDataSet = buckets.reduce(reducer, {
      dataSet: [],
      labels: []
    })
    setChartData(newDataSet.dataSet)
    setLabels(newDataSet.labels)
    console.log(buckets)
  }, [request])

  const updateChartAction = (data) => _ => {
    fetchData(data)
  }

  useEffect(() => {
    fetchData(body)

  }, [])

  return (
    <div>
      <div>
        <ChartStat
          labels={labels}
          data={chartData}
        />
      </div>
      <button
        onClick={updateChartAction(body1)}
      >
        Update chart 1
      </button>
      <button
        onClick={updateChartAction(body)}
      >
        Update chart 2
      </button>
    </div>
  );
}

export default App;


